from django.shortcuts import render, redirect
from django.db import connection

# Create your views here.


def dictfetchall(cursor): # Return all rows from a cursor as a dict
    columns = [col[0] for col in cursor.description]
    return [ dict(zip(columns, row)) for row in cursor.fetchall() ]

    
def home(request):
    if 'username' in request.session:
        uname = request.session['username'][0]
        peran = request.session['username'][1]

        if cekPeran(uname, peran) == 0:
            return render(request, 'home/index_public.html')

        elif cekPeran(uname, peran) == 1:
            return render(request, 'home/index_dokter.html')

        elif cekPeran(uname, peran) == 2:
            return render(request, 'home/index_satgas.html')

        else:
            return render(request, 'home/index_sistem.html')
    
    
    else:
        return redirect('/satu/login')


def cekPeran(uname, peran): # Returns 0 if user is a pengguna, 1 if user is a dokter, 2 if user is an admin satgas
    unameBuatSql = "'" + uname + "'"

    if peran == "pengguna":
        return 0  
    
    elif peran == "sistem":
        return 3

    else:
        x = connection.cursor()    
        x.execute('SELECT NoSTR FROM siruco.dokter WHERE username = ' + unameBuatSql + ';')
        temp = dictfetchall(x)

        if len(temp) > 0:
            return 1

        else:
            return 2
    

def rumahSakits(request):
    uname = request.session['username'][0]
    peran = request.session['username'][1]

    if cekPeran(uname, peran) == 0:
        return render(request, 'home/rumahSakit_public.html')

    elif cekPeran(uname, peran) == 1:
        return render(request, 'home/rumahSakit_dokter.html')

    elif cekPeran(uname, peran) == 2:
        return render(request, 'home/rumahSakit_satgas.html')
    

def transaksi(request):
    uname = request.session['username'][0]
    peran = request.session['username'][1]

    if cekPeran(uname, peran) == 0:
        return render(request, 'home/transaksi_public.html')

    elif cekPeran(uname, peran) == 2:
        return render(request, 'home/transaksi_satgas.html')


def hotel(request):
    uname = request.session['username'][0]
    peran = request.session['username'][1]

    if cekPeran(uname, peran) == 0:
        return render(request, 'home/hotel_public.html')

    elif cekPeran(uname, peran) == 3:
        return render(request, 'home/hotel_sistem.html')

    elif cekPeran(uname, peran) == 2:
        return render(request, 'home/hotel_satgas.html')

def pasien(request):
    uname = request.session['username'][0]
    peran = request.session['username'][1]

    if cekPeran(uname, peran) == 0:
        return render(request, 'home/pasien_public.html')

    elif cekPeran(uname, peran) == 1:
        return render(request, 'home/pasien_dokter.html')

    elif cekPeran(uname, peran) == 2:
        return render(request, 'home/pasien_satgas.html')


def faskes(request):
    return render(request, 'home/faskes_satgas.html')


