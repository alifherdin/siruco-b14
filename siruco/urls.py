from django.urls import path

from . import views

app_name = 'siruco'

urlpatterns = [
    path('',views.home,name='home'),
    path('home', views.home, name = 'home'),
    path('home/rumahSakit', views.rumahSakits, name = 'rumahSakit'),
    path('home/faskes', views.faskes, name = 'faskes'),
    path('home/hotel', views.hotel, name = 'hotel'),
    path('home/pasien', views.pasien, name = 'pasien'),
    path('home/transaksi', views.transaksi, name = 'transaksi'),
]