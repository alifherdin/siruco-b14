from django.apps import AppConfig


class SirucoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'siruco'
