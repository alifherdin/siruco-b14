from django.shortcuts import render
from . import models
from django.db import connection
from django.shortcuts import redirect
from django.contrib.sessions.models import Session
from lima.forms import *

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def listTransaksiMakan(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM SIRUCO.TRANSAKSI_MAKAN")
        row = dictfetchall(cursor)
        context = {
            "list_transaksi_makan": row,
            
        }
        return render(request, 'list_transaksi_makan.html', context)
    
    return render(request,"list_transaksi_makan.html",{})

def detailTransaksiMakan(request, idtransaksimakanan):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM SIRUCO.TRANSAKSI_MAKAN AS TM WHERE TM.idTransaksiMakanan = %s", [idtransaksimakanan])
        row = dictfetchall(cursor)
        cursor.close()
        cursor.execute("SELECT * FROM SIRUCO.DAFTAR_PESAN AS DP JOIN SIRUCO.PAKET_MAKAN AS PM ON DP.KodePaket = PM.KodePaket WHERE DP.idTransaksiMakan = %s", [idtransaksimakanan])
        daftar_pesanan = dictfetchall(cursor)
        cursor.close()
        context = {
            "detail_transaksi_makan": row,
            "daftar_pesanan": daftar_pesanan,
        }
        return render(request, 'detail_transaksi_makan.html', context)
    
    return render(request,"detail_transaksi_makan.html",{})

def listPaketMakan(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM SIRUCO.PAKET_MAKAN")
        row = dictfetchall(cursor)
        context = {
            "detail_paket_makan": row,
            
        }
        return render(request, 'list_paket_makan.html', context)
    
    return render(request,"list_paket_makan.html",{})

def listHotel(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM SIRUCO.HOTEL")
        row = dictfetchall(cursor)
        context = {
            "hotel": row, 
        }
        # print(row)
        return render(request, 'list_hotel.html', context)
    
    return render(request,"list_hotel.html",{})

# Untuk mendapatkan kode hotel terakhir
def getKodeHotel():
    with connection.cursor() as cursor:
        cursor.execute("SET search_path TO SIRUCO")
        cursor.execute("SELECT kode FROM hotel ORDER BY kode DESC LIMIT 1")
        last = cursor.fetchall()[0][0]
        print(last)
        
        new = int(last[1:])+1
        kode_hotel = "1"+str(new).zfill(2)
    return kode_hotel

def createHotel(request):
    kode = getKodeHotel()
    # if request.session.has_key('role'):
    #     if request.session['role'] == 0:
    form = createHotelForm(initial={'kode_hotel' : kode},data=request.POST or None)
    response = {}
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return redirect('lima:listHotel')
        else: 
            return redirect('lima:listHotel')
    response.update(form=form)
    return render(request, 'create_hotel.html', response)
    # return redirect('homepage:homepage')
    

def updateHotel(request, kode):
    # if request.session.has_key('role'):
    #     if request.session['role'] == 0:
    form = updateHotelForm(kode=kode, data=request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return redirect('lima:listHotel')
        elif form.is_valid() == None:
            return redirect('lima:listHotel')
    response = {
        'form':form
    }
    return render(request, 'update_hotel.html', response)
    # return redirect('homepage:homepage')


