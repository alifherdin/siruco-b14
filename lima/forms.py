from django import forms
from django.db import connection


class createHotelForm(forms.Form):
    attrs = {'class': 'form-control'}
    disabled_attrs = {'class': 'form-control', 'readonly':'readonly'}
    
    kode_hotel = forms.CharField(label="Kode Hotel", required=True, widget=forms.TextInput(attrs=disabled_attrs))
    nama = forms.CharField(label="Nama Hotel", required=True, widget=forms.TextInput(attrs=attrs))
    is_rujukan = forms.BooleanField(label="Rujukan", required=False, widget=forms.CheckboxInput(attrs=attrs))
    jalan = forms.CharField(label="Jalan", required=True, widget=forms.TextInput(attrs=attrs))
    kelurahan = forms.CharField(label="Kelurahan", required=True, widget=forms.TextInput(attrs=attrs))
    kecamatan = forms.CharField(label="Kecamatan", required=True, widget=forms.TextInput(attrs=attrs))
    kab_kot = forms.CharField(label="Kabupaten/Kota", required=True, widget=forms.TextInput(attrs=attrs))
    provinsi = forms.CharField(label="Provinsi", required=True, widget=forms.TextInput(attrs=attrs))

    def save(self, commit=True):

        kode = self.cleaned_data.get('kode_hotel')
        nama = self.cleaned_data.get('nama')
        is_rujukan = self.cleaned_data.get('is_rujukan')
        if is_rujukan == True:
            is_rujukan = "1"
        else:
            is_rujukan = "0"
        jalan = self.cleaned_data.get('jalan')
        kelurahan = self.cleaned_data.get('kelurahan')
        kecamatan = self.cleaned_data.get('kecamatan')
        kab_kot = self.cleaned_data.get('kab_kot')
        provinsi = self.cleaned_data.get('provinsi')
        
        with connection.cursor() as cursor:
            cursor.execute('insert into siruco.hotel values(%s, %s, %s, %s, %s, %s, %s, %s)', 
            [ 
                kode,
                nama,
                is_rujukan,
                jalan,
                kelurahan,
                kecamatan,
                kab_kot,
                provinsi
            ])

class updateHotelForm(forms.Form):
    def __init__(self, *args, **kwargs):
        kode = kwargs.pop('kode')
        super(updateHotelForm, self).__init__(*args, **kwargs)

        with connection.cursor() as cursor:
            cursor.execute('select * from siruco.hotel where kode = %s', [kode])
            
            hotel_object = cursor.fetchall()

            self.fields['kode_hotel'].initial = kode
            self.fields['nama'].initial = hotel_object[0][1]
            self.fields['is_rujukan'].initial = hotel_object[0][2]
            self.fields['jalan'].initial = hotel_object[0][3]
            self.fields['kelurahan'].initial = hotel_object[0][4]
            self.fields['kecamatan'].initial = hotel_object[0][5]
            self.fields['kab_kot'].initial = hotel_object[0][6]
            self.fields['provinsi'].initial = hotel_object[0][7]


    attrs = {'class': 'form-control'}
    disabled_attrs = {'class': 'form-control', 'readonly':'readonly'}

    kode_hotel = forms.CharField(label="Kode Hotel", required=True, widget=forms.TextInput(attrs=disabled_attrs))
    nama = forms.CharField(label="Nama Hotel", required=True, widget=forms.TextInput(attrs=attrs))
    is_rujukan = forms.BooleanField(label="Rujukan", required=False, widget=forms.CheckboxInput(attrs=attrs))
    jalan = forms.CharField(label="Jalan", required=True, widget=forms.TextInput(attrs=attrs))
    kelurahan = forms.CharField(label="Kelurahan", required=True, widget=forms.TextInput(attrs=attrs))
    kecamatan = forms.CharField(label="Kecamatan", required=True, widget=forms.TextInput(attrs=attrs))
    kab_kot = forms.CharField(label="Kabupaten/Kota", required=True, widget=forms.TextInput(attrs=attrs))
    provinsi = forms.CharField(label="Provinsi", required=True, widget=forms.TextInput(attrs=attrs))

    def save(self, commit=True):
        kode = self.cleaned_data.get('kode_hotel')
        nama = self.cleaned_data.get('nama')
        is_rujukan = self.cleaned_data.get('is_rujukan')
        if is_rujukan == True:
            is_rujukan = "1"
        else:
            is_rujukan = "0"
        jalan = self.cleaned_data.get('jalan')
        kelurahan = self.cleaned_data.get('kelurahan')
        kecamatan = self.cleaned_data.get('kecamatan')
        kab_kot = self.cleaned_data.get('kab_kot')
        provinsi = self.cleaned_data.get('provinsi')
        
        with connection.cursor() as cursor:
            cursor.execute('update siruco.hotel set nama = %s, isrujukan = %s, jalan = %s, kelurahan = %s, kecamatan = %s, kabkot = %s, prov = %s where hotel.kode = %s', 
            [ 
                nama,
                is_rujukan,
                jalan,
                kelurahan,
                kecamatan,
                kab_kot,
                provinsi,
                kode
            ])