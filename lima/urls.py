from django.urls import path
from . import views

app_name='lima'

urlpatterns = [
    path('transaksi_makan/list_transaksi_makan/', views.listTransaksiMakan, name='listTransaksiMakan'),
    path('hotel/list_paket_makan/', views.listPaketMakan, name='listPaketMakan'),
    path('hotel/list_hotel/', views.listHotel, name='listHotel'),
    path('hotel/buat_hotel/', views.createHotel, name='createHotel'),
    path('hotel/update_hotel/<kode>', views.updateHotel, name='updateHotel'),
    path('transaksi_makan/detail_transaksi_makan/<idtransaksimakanan>', views.detailTransaksiMakan, name='detailTransaksiMakan'),
]