from django.urls import path

from . import views

app_name = 'satu'

urlpatterns = [
    path('login', views.login, name = 'login'),
    path('logout', views.logout, name = 'logout'),
    path('registrasi',views.registrasi, name = 'registrasi')
]