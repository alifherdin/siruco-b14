from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.db import connection

# Create your views here.

def login(request):
    if request.method == 'POST':
        return cekUserPass(request)

    return render(request, "login.html")


def logout(request):
    if 'username' in request.session:
        try:
            del request.session['username']
        except KeyError:
            pass
        return redirect('/satu/login')

    else:
        return redirect('/satu/login')

def registrasi(request):
    x = connection.cursor()
    x.execute("SELECT kode from siruco.faskes")
    daftar_faskes = dictfetchall(x)
    if request.method == 'POST' :
        peran = request.POST.get('peran')
        username = request.POST.get('username')
        password = request.POST.get('password')
        nama = request.POST.get('nama')
        nik = request.POST.get('nik')
        nohp = request.POST.get('nohp')
        gelardepan = request.POST.get('gelardepan')
        gelarbelakang = request.POST.get('gelarbelakang')
        faskes = request.POST.get('faskes')
        role_peran = "admin"
        if (peran == "0"):
            role_peran = "pengguna"
        else:
            x.execute("insert into admin values(%s)",[username])
        x.execute("insert into akun_pengguna values (%s,%s,%s)",[username,password,role_peran])
        if(peran == "0"):
             x.execute("insert into pengguna_publik values (%s,%s,%s,AKTIF,penanggungjawab,%s)",[username,nik,nama,nohp])
        elif(peran == "1"):
             x.execute("insert into dokter values (%s,%s,%s,%s,%s,%s)",[username,nostr,nama,nohp,gelardepan,gelarbelakang])
        elif(peran == "2"):
             x.execute("insert into admin_satgas values (%s,%s)",[username,faskes])
        return redirect('siruco:home')
    return render(request,"registrasi.html",{'daftar_faskes':daftar_faskes})




def dictfetchall(cursor): # Return all rows from a cursor as a dict
    columns = [col[0] for col in cursor.description]
    return [ dict(zip(columns, row)) for row in cursor.fetchall() ]


def cekUserPass(req):
    args = {}

    uname = "'" + req.POST.get("username", "") + "'"
    pswd = "'" + req.POST.get("password", "") + "'"

    crs_uname = connection.cursor()
    crs_pswd = connection.cursor()

    crs_uname.execute('SELECT username FROM siruco.akun_pengguna WHERE username = ' + uname + ';')
    una = dictfetchall(crs_uname)
    jml_uname = len(una)

    if jml_uname == 1:
        teks = 'SELECT peran FROM siruco.akun_pengguna WHERE username = '
        crs_pswd.execute(teks + uname + ' AND password = ' + pswd + ';')

        pna = dictfetchall(crs_pswd)
        jml_pswd = len(pna)

        if jml_pswd == 0:
            args['result'] = "Password anda salah"

        elif jml_pswd == 1:
            args['result'] = pna[0].get('peran')
            req.session['username'] = [una[0].get('username'), pna[0].get('peran')]

            return redirect('/home')

    elif jml_uname == 0:
        args['result'] = "Username anda salah"

    return render(req, "login.html", args)
