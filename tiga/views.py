from django.shortcuts import render, redirect
from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def CreateJadwalFaskes(request):
    x = connection.cursor()
    if request.method == 'POST' :
        faskes = request.POST.get('faskes')
        shift = request.POST.get('shift')
        tanggal = request.POST.get('tanggal')
        x.execute("select kode from siruco.faskes where nama = %s",[faskes])
        kode_faskes = namedtuplefetchall(x)[0]
        x.execute("INSERT INTO jadwal(tanggal, shift, kode_faskes) VALUES (%s,%s,%s)",[tanggal,shift,kode_faskes])
        return redirect('tiga:CreateJadwalFaskes')
    x.execute("SELECT nama from siruco.faskes")
    daftar_faskes = namedtuplefetchall(x)
    return render(request,'9-createjadwal.html',{'daftar_faskes':daftar_faskes})

def ViewJadwalFaskes(request):
    x = connection.cursor()
    x.execute("SELECT nama,shift,tanggal from siruco.JADWAL,siruco.faskes where kode_faskes = kode")
    listjadwalfaskes = namedtuplefetchall(x)
    return render(request,'9-viewjadwal.html',{"listjadwalfaskes":listjadwalfaskes})

def CreateRS(request):
    x = connection.cursor()
    if request.method == 'POST' :
        faskes = request.POST.get('faskes')
        rujukan = request.POST.get('rujukan')
        # x.execute("select kode from siruco.faskes where nama = %s",[faskes])
        # kode_faskes = namedtuplefetchall(x)[0]
        x.execute("INSERT INTO siruco.rumah_sakit VALUES (%s,%s)",[faskes,rujukan])
        return redirect('tiga:ViewRS')
    x.execute("SELECT kode from siruco.faskes")
    daftar_faskes = namedtuplefetchall(x)
    return render(request,'10-creaters.html',{'daftar_faskes':daftar_faskes})

def ViewRS(request):
    x = connection.cursor()
    x.execute("SELECT * from siruco.rumah_sakit")
    listrumahsakit = namedtuplefetchall(x)
    return render(request,'10-viewrs.html',{"listrumahsakit":listrumahsakit})

def UpdateRS(request,idx):
    x = connection.cursor()
    kode_faskes = idx
    x.execute("select * from siruco.rumah_sakit where kode_faskes = %s",[kode_faskes])
    daftar_faskes = namedtuplefetchall(x)
    if request.method == 'POST' :
        faskes = request.POST.get('faskes')
        rujukan = request.POST.get('rujukan')
        if (rujukan is None):
            rujukan = '0'
        x.execute("UPDATE siruco.rumah_sakit SET isrujukan = %s WHERE kode_faskes = %s",[rujukan,faskes])
        return redirect('tiga:ViewRS')
    return render(request,'10-updaters.html',{"daftar_faskes":daftar_faskes})

def ViewTransRS(request):
    x = connection.cursor()
    x.execute("SELECT * from siruco.transaksi_rs")
    listtransaksi = namedtuplefetchall(x)
    return render(request,'11-viewtransrs.html',{"listtransaksi":listtransaksi})

def UpdateTransRS(request,idx):
    x = connection.cursor()
    idtrans = idx
    x.execute("select * from siruco.transaksi_rs where idtransaksi = %s",[idtrans])
    transrs = namedtuplefetchall(x)
    if request.method == 'POST' :
        idtransaksi = request.POST.get('idtransaksi')
        kodepasien = request.POST.get('kodepasien')
        tanggalpembayaran = request.POST.get('tanggalpembayaran')
        waktupembayaran = request.POST.get('waktupembayaran')
        tglmasuk = request.POST.get('tglmasuk')
        totalbiaya = request.POST.get('totalbiaya')
        statusbayar = request.POST.get('statusbayar')
        x.execute("UPDATE siruco.transaksi_rs SET statusbayar = %s WHERE idtransaksi = %s",[statusbayar,idtransaksi])
        return redirect('tiga:ViewTransRS')
    return render(request,'11-updatetransrs.html',{"transrs":transrs})

def DeleteTransRS(request,idx):
    x = connection.cursor()
    idtrans = idx
    x.execute("delete from siruco.transaksi_rs where idtransaksi = %s",[idtrans])
    return redirect('/tiga/viewtransrs')