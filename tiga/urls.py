from django.urls import path

from . import views

app_name = 'tiga'

urlpatterns = [
    path('createjadwal', views.CreateJadwalFaskes, name = 'CreateJadwalFaskes'),
    path('viewjadwal', views.ViewJadwalFaskes, name = 'ViewJadwalFaskes'),
    path('creaters',views.CreateRS, name = 'CreateRS'),
    path('viewrs',views.ViewRS, name = 'ViewRS'),
    path('updaters/<str:idx>',views.UpdateRS, name = 'UpdateRS'),
    path('viewtransrs',views.ViewTransRS, name= 'ViewTransRS'),
    path('updatetransrs/<str:idx>',views.UpdateTransRS,name= 'UpdateTransRs'),
    path('deletetransrs/<str:idx>',views.DeleteTransRS,name= 'DeleteTransRs')
]