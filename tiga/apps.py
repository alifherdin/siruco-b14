from django.apps import AppConfig


class TigaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tiga'
