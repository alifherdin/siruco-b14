from django.urls import path

from . import views

app_name = 'dua'

urlpatterns = [
    path('buatJaDok', views.buatJadwalDokter, name = 'buatJadwalDokter'),
    path('createJaDok/<int:idx>', views.createJadwalDokter, name = 'createJadwalDokter'),
    path('showJaDok', views.showJadwalDokter, name = 'showJadwalDokter'),
    path('reserveDokter', views.tambahReservasi, name = 'tambahReservasi'),
    path('createReservasi/<int:idx>', views.createReservasi, name = 'createreservasi'),
    path('seeAppointment', views.seeMemeriksa, name = 'seeAppointment'),
    path('updateAppointment/<int:idx>', views.updateMemeriksa, name = 'updateAppointment'),
    path('deleteAppointment/<int:idx>', views.deleteMemeriksa, name = 'deleteAppointment'),
    path('buatRuangan', views.newRuangan, name = 'buatRuangan'),
    # path('buatBed', views.newBed, name = 'buatBed'),
]