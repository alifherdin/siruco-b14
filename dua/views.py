from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.db import connection, InternalError
from . import mimimi, mumumu, mememe

# Create your views here.

jadwals = {}

def buatJadwalDokter(request):
    uname = request.session['username'][0]
    peran = request.session['username'][1]

    isDokter = mimimi.cekAdmin(uname, peran)
    
    jadwals['listJadwal'] = mimimi.showJadwalUtkDibuat(uname)

    if isDokter:
        return render(request, "nomer4/buatJD.html", jadwals)
    
    else:
        return render(request, "nomer4/buatJD_nondokter.html", jadwals)


def createJadwalDokter(request, idx):
    uname = request.session['username'][0]
    target = {}

    for i in jadwals['listJadwal']:
        if i['urutan'] == idx:
            target = i
            mimimi.addJadwal(uname, target)

    return redirect('/dua/showJaDok')


def showJadwalDokter(request):
    uname = request.session['username'][0]
    peran = request.session['username'][1]

    isDokter = mimimi.cekAdmin(uname, peran)

    if isDokter:
        jadwals['listJadwal'] = mimimi.showJadwalDokter(uname, True)
        return render(request, "nomer4/showJD.html", jadwals)
    
    else:
        jadwals['listJadwal'] = mimimi.showJadwalDokter(uname, False)
        return render(request, "nomer4/showJD_nondokter.html", jadwals)


def tambahReservasi(request):
    uname = request.session['username'][0]
    peran = request.session['username'][1]

    jadwals['listJadwalDokter'] = mumumu.ambilJadwalDokter(uname, peran)
    
    return render(request, "nomer5/resJD.html", jadwals)


def createReservasi(request, idx):
    if request.method == 'GET':
        uname = request.session['username'][0]
        peran = request.session['username'][1]
        target = {}

        for i in jadwals['listJadwalDokter']:
            if i['urutan'] == idx:
                target = i

        jadwals['NIKs'] = mumumu.ambilNIK(uname, peran)
        jadwals['untukForm'] = target

        tempTanggal = str(jadwals['untukForm']['tanggal'])
        jadwals['untukForm']['tanggal'] = tempTanggal

        return render(request, "nomer5/formMemeriksa.html", jadwals)
    
    elif request.method == 'POST':
        target = request.POST
        
        try:
            mumumu.addMemeriksa(target)
        except InternalError as e:
            return render(request, "nomer5/formError.html")

        return render(request, "nomer5/formSuccess.html")


def seeMemeriksa(request):
    uname = request.session['username'][0]
    peran = request.session['username'][1]

    jadwals['listAppointment'] = mumumu.lihatAppointment(uname, peran)

    for i in jadwals['listAppointment']:
        tempTanggal = i['praktek_tgl']

        i['praktek_tgl'] = str(tempTanggal)

    if mumumu.cekPeran(uname, peran) == 0:
        return render(request, "nomer5/jadwalAppointment_public.html", jadwals)
    
    elif mumumu.cekPeran(uname, peran) == 1:
        return render(request, "nomer5/jadwalAppointment_dokter.html", jadwals)

    elif mumumu.cekPeran(uname, peran) == 2:
        return render(request, "nomer5/jadwalAppointment_admin.html", jadwals)


def updateMemeriksa(request, idx):
    if request.method == 'GET':
        jadwals['appointmentUpdate'] = {}

        for i in jadwals['listAppointment']:
                if i['urutan'] == idx:
                    jadwals['appointmentUpdate'] = i
                    print (i)
                    break
        
        return render(request, "nomer5/updateAppointment.html", jadwals)
    
    elif request.method == 'POST':
        target = request.POST

        mumumu.updateAppointment(target)

        return redirect('/dua/seeAppointment')


def deleteMemeriksa(request, idx):
    for i in jadwals['listAppointment']:
            if i['urutan'] == idx:
                target = i
                mumumu.deleteAppointment(target)
                break
    
    return redirect('/dua/seeAppointment')
    
        
def newRuangan(request):
    jadwals['daftarRS'] = mememe.getNoRuangan()

    if request.method == 'POST':
        target = request.POST
        jadwals['hasilAddRuangan'] = mememe.addRuangan(target)

        if jadwals['hasilAddRuangan'] == target['kodeRS'].split()[1]:
            return render(request, "nomer6/newRuangan_success.html", jadwals)

    return render(request, "nomer6/newRuangan.html", jadwals)


# def newBed(request):
#     jadwals['daftarBed'] = mememe.getNoBed()

#     # if request.method == 'POST':
#     #     target = request.POST
#     #     jadwals['hasilAddRuangan'] = mememe.addRuangan(target)

#     #     if jadwals['hasilAddRuangan'] == target['kodeRS'].split()[1]:
#     #         return render(request, "nomer6/newRuangan_success.html", jadwals)

#     return render(request, "nomer6/newBed.html", jadwals)

