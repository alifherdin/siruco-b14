from django.db import connection
from . import mimimi, mumumu



def dictfetchall(cursor): # Return all rows from a cursor as a dict
    columns = [col[0] for col in cursor.description]
    return [ dict(zip(columns, row)) for row in cursor.fetchall() ]


def getRS():
    x = connection.cursor()
    text = "SELECT kode_faskes FROM siruco.rumah_sakit;"
    x.execute(text)
    temp = dictfetchall(x)
    
    return temp


def getNoRuangan():
    listkodeRuangan = []
    listRS = getRS()

    for i in range (len(listRS)):
        it = listRS[i]['kode_faskes']
        x = connection.cursor()

        text = "SELECT MAX(koderuangan) FROM siruco.ruangan_rs WHERE koders = "
        text += "'" + it + "';"

        x.execute(text)
        temp = dictfetchall(x)

        if temp[0]['max'] != None:
            temp[0]['max'] = str(int(temp[0]['max']) + 1)
            temp[0]['kode'] = it

        else:
            temp[0]['kode'] = it
            temp[0]['max'] = '00001'
        
        listkodeRuangan.append(temp[0])

    return listkodeRuangan


def addRuangan(target):
    x = connection.cursor()
    kodeRS = target['kodeRS'].split()[0]
    kodeRuangan = target['kodeRS'].split()[1]
    tipe = target['tipe']
    harga = target['harga']

    text = "INSERT INTO siruco.ruangan_rs VALUES ("
    text += "'" + kodeRS + "', '" + kodeRuangan + "', '" + tipe + "', 0, " + harga + ");"

    x.execute(text)

    return kodeRuangan

def getNoRuanganBuatBed():
    listkodeRuangan = []
    listRS = getRS()

    for i in range (len(listRS)):
        it = listRS[i]['kode_faskes']
        x = connection.cursor()

        text = "SELECT koderuangan FROM siruco.ruangan_rs WHERE koders = "
        text += "'" + it + "';"

        x.execute(text)
        temp = dictfetchall(x)

        for i in range (len(temp)):
            if temp[i]['koderuangan'] != None:
                temp[i]['koderuangan'] = str(int(temp[i]['koderuangan']) + 1)
                temp[i]['kode'] = it
                listkodeRuangan.append(temp[i])

            else:
                temp[i]['kode'] = it
                temp[i]['koderuangan'] = '00001'
                listkodeRuangan.append(temp[i])
        
        

    return listkodeRuangan


def getNoBed():
    listRuangan = getNoRuanganBuatBed()

    for i in range (len(listRuangan)):
        if listRuangan[i]['koderuangan'] != '00001':
            listRuangan[i]['koderuangan'] = str(int(listRuangan[i]['koderuangan']) - 1)
            kru = listRuangan[i]['koderuangan']
            krs = listRuangan[i]['kode']
        
        else:
            listRuangan.pop(i)

        x = connection.cursor()

        text = "SELECT MAX(kodebed) bed FROM siruco.bed_rs WHERE koders = "
        text += "'" + krs + "' AND koderuangan = '" +  kru +  "';"

        x.execute(text)
        temp = dictfetchall(x)

        if temp[0]['bed'] != None:
            listRuangan[i]['bed'] = str(int(temp[0]['bed']) + 1)

        else:
            listRuangan[i]['bed'] = '00001'

        print (listRuangan[i])

    return listRuangan