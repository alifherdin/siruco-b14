from django.db import connection


def dictfetchall(cursor): # Return all rows from a cursor as a dict
    columns = [col[0] for col in cursor.description]
    return [ dict(zip(columns, row)) for row in cursor.fetchall() ]


def ambilSTR(uname):
    x = connection.cursor()
    unameBuatSql = "'" + uname + "'"

    x.execute('SELECT NoSTR FROM siruco.dokter WHERE username = ' + unameBuatSql + ';')
    temp = dictfetchall(x)
    

    if len(temp) > 0:
        result = temp[0].get('nostr')
        return result

    else:
        return None


def cekAdmin(uname, peran):
    x = ambilSTR(uname)

    if x != None:
        return True

    elif x == None:
        return False


def showJadwalUtkDibuat(uname):
    unameBuatSql = "'" + uname + "'"
    text1 = "SELECT * FROM siruco.jadwal EXCEPT "
    text2 = text1 + "SELECT kode_faskes, shift, tanggal FROM siruco.jadwal_dokter, siruco.dokter "
    text3 = text2 + "WHERE dokter.noSTR = jadwal_dokter.noSTR AND "
    text4 = text3 + "dokter.username = jadwal_dokter.username AND dokter.username = " + unameBuatSql + ";"

    x = connection.cursor()

    x.execute(text4)
    hasilCek = dictfetchall(x)

    jmlHasilCek = len(hasilCek)

    for i in range(jmlHasilCek):
        hasilCek[i]['urutan'] = i

    return hasilCek

def addJadwal(uname, target):
    x = connection.cursor()

    no_str = ambilSTR(uname)
    unameBuatSql = "'" + uname + "'"
    kodefaskes = target['kode_faskes']
    shift = "'" + target['shift'] + "'"
    tgl = "'" + str(target['tanggal']) + "'"

    text = "INSERT INTO siruco.JADWAL_DOKTER VALUES ("
    text += no_str + ", " + unameBuatSql + ", " + kodefaskes + ", " + shift + ", " + tgl + ", " + "0"
    text += ");"

    x.execute(text)


def showJadwalDokter(uname, isDokter):
    unameBuatSql = "'" + uname + "'"

    if isDokter:
        text1 = "SELECT siruco.jadwal_dokter.nostr, siruco.jadwal_dokter.username, kode_faskes, shift, "
        text2 = text1 + "tanggal, jmlpasien FROM siruco.jadwal_dokter, siruco.dokter "
        text3 = text2 + "WHERE dokter.noSTR = jadwal_dokter.noSTR AND "
        text4 = text3 + "dokter.username = jadwal_dokter.username AND dokter.username = " + unameBuatSql + ";"
    
    else:
        text4 = "SELECT * FROM siruco.jadwal_dokter;"

    x = connection.cursor()

    x.execute(text4)

    hasilCek = dictfetchall(x)

    jmlHasilCek = len(hasilCek)

    for i in range(jmlHasilCek):
        hasilCek[i]['urutan'] = i

    return hasilCek
