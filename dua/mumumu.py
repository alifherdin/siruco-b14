from django.db import connection
from . import mimimi


def dictfetchall(cursor): # Return all rows from a cursor as a dict
    columns = [col[0] for col in cursor.description]
    return [ dict(zip(columns, row)) for row in cursor.fetchall() ]


def ambilNIK(uname, peran):
    x = connection.cursor()
    unameBuatSql = "'" + uname + "'"
    daftarNIK = []

    if cekPeran(uname, peran) == 0:
        text = "SELECT NIK FROM siruco.PASIEN WHERE idpendaftar = " + unameBuatSql + ";"
    
    elif cekPeran(uname, peran) == 2:
        text = "SELECT NIK FROM siruco.PASIEN;"

    x.execute(text)

    for i in (dictfetchall(x)):
        daftarNIK.append(i['nik'])
    
    return daftarNIK


def ambilSTR(uname):
    x = connection.cursor()
    unameBuatSql = "'" + uname + "'"

    x.execute('SELECT NoSTR FROM siruco.dokter WHERE username = ' + unameBuatSql + ';')
    temp = dictfetchall(x)
    

    if len(temp) > 0:
        result = temp[0].get('nostr')
        return result

    else:
        return None


def cekPeran(uname, peran): # Returns 0 if user is a pengguna, 1 if user is a dokter, 2 if user is an admin satgas
    unameBuatSql = "'" + uname + "'"

    if peran == "pengguna":
        return 0  

    elif peran == "sistem":
        return 3

    else:
        x = connection.cursor()    
        x.execute('SELECT NoSTR FROM siruco.dokter WHERE username = ' + unameBuatSql + ';')
        temp = dictfetchall(x)

        if len(temp) > 0:
            return 1

        else:
            return 2
    

def ambilJadwalDokter(uname, peran):
    x = connection.cursor()
    unameBuatSql = "'" + uname + "'"
    text = "SELECT * FROM siruco.jadwal_dokter;"

    x.execute(text)
    hasilCek = dictfetchall(x)
    jmlHasilCek = len(hasilCek)

    for i in range(jmlHasilCek):
        hasilCek[i]['urutan'] = i

    return hasilCek


def addMemeriksa(target):
    x = connection.cursor()

    nik_pasien = target['nikselect']
    unameDokter = "'" + target['unameDokter'] + "'"
    no_str = ambilSTR(target['unameDokter'])
    kodefaskes = target['kodeFaskes']
    shift = "'" + target['shift'] + "'"
    tgl = "'" + str(target['tanggal']) + "'"

    text = "INSERT INTO siruco.MEMERIKSA (nik_pasien, username_dokter, nostr, kode_faskes, praktek_shift, praktek_tgl) VALUES ("
    text += nik_pasien + ", " + unameDokter + ", " + no_str + ", " + kodefaskes + ", " + shift + ", " + tgl
    text += ");"

    x.execute(text)

    print("Sukses!")


def lihatAppointment(uname, peran):
    x = connection.cursor()
    unameBuatSql = "'" + uname + "'"

    if cekPeran(uname, peran) == 0:
        text = "select DISTINCT nik_pasien, nostr, username_dokter, kode_faskes,praktek_shift, praktek_tgl, rekomendasi "
        text += "from siruco.memeriksa m, siruco.pasien p, siruco.pengguna_publik pp "
        text += "where m.nik_pasien = p.nik and p.idpendaftar = "
        text += unameBuatSql + ";"

    elif cekPeran(uname, peran) == 1:
        text = "SELECT * FROM siruco.memeriksa "
        text += "where siruco.memeriksa.nostr = "
        text += "'" + ambilSTR(uname) + "';"
    
    elif cekPeran(uname, peran) == 2:
        text = "SELECT * FROM siruco.memeriksa;"

    x.execute(text)
    temp = dictfetchall(x)

    for i in range (len(temp)):
        xx = temp[i]
        xx['urutan'] = i + 1

    return temp


def updateAppointment(target):
    x = connection.cursor()

    nik_pasien = "'" +  target['nikpasien'] + "'"
    unameDokter = "'" + target['unameDokter'] + "'"
    kodefaskes = "'" + target['kodeFaskes'].split()[1] + "'"
    shift = "'" + target['shift'] + "'"
    tgl = "'" + target['tanggal'] + "'"
    rekomendasi =  "'" + target['rekomendasi'] + "'"

    text = "UPDATE siruco.memeriksa "
    text += "SET rekomendasi = " + rekomendasi + " "
    text += "WHERE nik_pasien = " +  nik_pasien + " AND username_dokter = " + unameDokter
    text += " AND kode_faskes = " + kodefaskes + " AND praktek_shift =  " + shift + " AND praktek_tgl =  " + tgl
    text += ";"

    x.execute(text)


def deleteAppointment(target):
    x = connection.cursor()

    nik_pasien = "'" +  target['nik_pasien'] + "'"
    unameDokter = "'" + target['username_dokter'] + "'"
    kodefaskes = "'" + target['kode_faskes'] + "'"
    shift = "'" + target['praktek_shift'] + "'"
    tgl = "'" + target['praktek_tgl'] + "'"

    text = "DELETE FROM siruco.memeriksa "
    text += "WHERE nik_pasien = " +  nik_pasien + " AND username_dokter = " + unameDokter
    text += " AND kode_faskes = " + kodefaskes + " AND praktek_shift =  " + shift + " AND praktek_tgl =  " + tgl
    text += ";"

    x.execute(text)