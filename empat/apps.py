from django.apps import AppConfig


class EmpatConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'empat'
