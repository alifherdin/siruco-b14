from django.urls import path

from . import views

app_name = 'empat'

urlpatterns = [
    path('readRuanganHotel', views.ReadRuanganHotel, name = 'ReadRuanganHotel'),
    path('createRuanganHotel', views.CreateRuanganHotel, name = 'CreateRuanganHotel'),
    path('readReservasiHotel', views.ReadReservasiHotel, name = 'ReadReservasiHotel'),
    path('readTransaksiHotel', views.ReadTransaksiHotel, name = 'ReadTransaksiHotel'),
    path('readTransaksiBooking', views.ReadTransaksiBooking, name = 'ReadTransaksiBooking'),
]