from django.shortcuts import render, redirect
from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def ReadRuanganHotel (request):
    x = connection.cursor()
    x.execute("SELECT * from siruco.HOTEL_ROOM")
    listRuanganHotel = namedtuplefetchall(x)
    return render(request,'12-read_ruanganHotel.html',{"listRuanganHotel":listRuanganHotel})

def ReadReservasiHotel (request):
    x = connection.cursor()
    x.execute("SELECT * from siruco.RESERVASI_HOTEL")
    listReservasiHotel = namedtuplefetchall(x)
    return render(request,'13-read_reservasiHotel.html',{"listReservasiHotel":listReservasiHotel})

def ReadTransaksiHotel (request):
    x = connection.cursor()
    x.execute("SELECT * from siruco.TRANSAKSI_HOTEL")
    listTransaksiHotel = namedtuplefetchall(x)
    return render(request,'14-read_transaksiHotel.html',{"listTransaksiHotel":listTransaksiHotel})

def ReadTransaksiBooking (request):
    x = connection.cursor()
    x.execute("SELECT * from siruco.TRANSAKSI_BOOKING")
    listTransaksiBooking = namedtuplefetchall(x)
    return render(request,'15-read_transaksiBooking.html',{"listTransaksiBooking":listTransaksiBooking})

def CreateRuanganHotel(request):
    x = connection.cursor()
    if request.method == 'POST' :
        KodeHotel = request.POST.get('KodeHotel')
        KodeRoom = request.POST.get('KodeRoom')
        JenisBed = request.POST.get('JenisBed')
        Tipe = request.POST.get('Tipe')
        Harga = request.POST.get('Harga')
        x.execute("select kode from siruco.HOTEL_ROOM where nama = %s",[])
        kode_faskes = namedtuplefetchall(x)[0]
        x.execute("INSERT INTO jadwal(KodeHotel, KodeRoom, JenisBed, Tipe, Harga) VALUES (%s,%s,%s,%s,%s)",[KodeHotel, KodeRoom, JenisBed, Tipe, Harga])
        return redirect('empat:CreateRuanganHotel')
    x.execute("SELECT KodeHotel from siruco.HOTEL_ROOM")
    daftar_ruanganHotel = namedtuplefetchall(x)
    return render(request,'12-create_ruanganHotel.html',{'daftar_ruanganHotel':daftar_ruanganHotel})

#def CreateReservasiHotel(request):

#def UpdateRuanganHotel(request):

#def UpdateRuanganHotel(request):
